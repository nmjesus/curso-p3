class Item {
  let price: Double;
  let name: String;

  init(price: Double, name: String) {
    self.price = price;
    self.name = name;
  }
}

class Machine {
  enum StateMachine {
    case INACTIVE
    case PAYING
    case DISPENSING
    case FINISHED
  }

  let items: [Item];
  var total = 0.0;
  var selected: Item?;
  var currentState = StateMachine.INACTIVE;

  init (items: [Item]) {
    self.items = items;
  }

  private func makeCoffee() {
    if let selected = self.selected {
      if (selected.price <= self.total) {
        self.currentState = StateMachine.DISPENSING;
      }
    }
  }

  private func canInteract() -> Bool {
    if (self.currentState != StateMachine.INACTIVE &&
        self.currentState != StateMachine.PAYING) {
      return false;
    }
    return true;
  }

  func insertCoin(_ value: Double) {
    if (!self.canInteract()) {
      return;
    }

    self.currentState = StateMachine.PAYING;
    self.total += value;

    self.makeCoffee();
  }

  func itemSelection(_ productName: String) {
    if (!self.canInteract()) {
      return;
    }

    for item in self.items {
      if (item.name == productName) {
        self.selected = item;
      }
    }

    if (self.selected == nil) {
      print("Please select one item");
    }

    self.makeCoffee();
  }

  func finishFilling() -> Double? {
    if (self.total > 0 && self.total < self.selected!.price) {
      return nil;
    }

    let total = self.total;
    let item = self.selected;

    self.currentState = StateMachine.FINISHED;
    self.total = 0.0;

    return total - item!.price;
  }

  func clientPickedUpCup() {
    self.selected = nil;
    self.currentState = StateMachine.INACTIVE;
  }
}

let machine = Machine(items: [
  Item(price: 0.5, name: "Café Lungo"),
  Item(price: 0.55, name: "Decaf"),
  Item(price: 0.80, name: "Double"),
  Item(price: 0.4, name: "Short")
]);


func round(_ value: Double) -> Double {
  var v = value * 100;
  v.round();

  return v/100;
}


// print("TEST CASE 1");
// machine.insertCoin(0.2);
// machine.insertCoin(0.3);
// machine.itemSelection("Café Lungo");
// if let change = machine.finishFilling() {
//   print("your change: \(round(change))", "\nyour coffe: \(machine.selected?.name ?? "")");
//   machine.clientPickedUpCup();
// };
// 
// print("=======================================================");
// 
// print("TEST CASE 2");
// machine.itemSelection("Café Lungo");
// machine.insertCoin(0.2);
// machine.insertCoin(0.5);
// if let change = machine.finishFilling() {
//   machine.insertCoin(0.5);
//   print("your change: \(round(change))", "\nyour coffe: \(machine.selected?.name ?? "")");
//   machine.clientPickedUpCup();
// };
// 
// let pi: Double = 3.14159265358979
// print("%.2f", pi);
