class ParkingLot {
  var cars: [Car] = [];

  func buyTicket(qty: Int, onBuy: (_: Int) -> ()) {
    onBuy(qty);
  }

  func addCar(car: Car, onEnterParkingLot: (_: Car) -> ()) {
    if car.hasTickets() {
      self.cars.append(car);
      onEnterParkingLot(car);
    } else {
      print("\(car.licensePlate) does not have a valid ticket");
    }
  }

  func searchCar(licensePlate: String) -> Car? {
    for car in self.cars {
      if car.licensePlate == licensePlate {
        return car;
      }
    }
    return nil;
  }
}

class Car {
  let licensePlate: String;
  private var tickets: Int = 0;

  init(licensePlate: String, tickets: Int = 0) {
    self.licensePlate = licensePlate;
    self.tickets = tickets;
  }

  func addTicket(qty: Int) {
    self.tickets += qty;
  }

  func useTicket() {
    self.tickets -= 1;
  }

  func hasTickets() -> Bool {
    return self.tickets > 0;
  }
};


// let car1 = Car(licensePlate: "aa-00-22");
// let car2 = Car(licensePlate: "aa-00-23");
// let car3 = Car(licensePlate: "aa-00-24");
// let park = ParkingLot();
// 
// park.buyTicket(qty: 2, onBuy: {(qty) -> () in car1.addTicket(qty: qty); });
// park.buyTicket(qty: 1, onBuy: {(qty) -> () in car2.addTicket(qty: qty); });
// 
// park.addCar(car: car1, onEnterParkingLot: {(car) -> () in car.useTicket(); });
// park.addCar(car: car2, onEnterParkingLot: {(car) -> () in car.useTicket(); });
// park.addCar(car: car3, onEnterParkingLot: {(car) -> () in car.useTicket(); });
