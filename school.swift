class Classroom {
  let capacity: Int;
  let name: String;
  let number: Int;

  var students: [Student] = [];

  init(capacity: Int, name: String, number: Int) {
    self.capacity = capacity;
    self.name = name;
    self.number = number;
  }

  func addStudent(_ student: Student) -> Bool {
    if (self.students.count < self.capacity) {
      self.students.append(student);
      return true;
    }
    return false;
  }

  func removeStudent(_ number: Int) -> Bool {
    var idx = -1;
    for (index, student) in self.students.enumerated() {
      if (student.number == number) {
        idx = index;
      }
    }

    if (idx != -1) {
      self.students.remove(at: idx);
      return true;
    }

    return false;
  }

  func list() {
    for student in self.students {
      print (student.name, "::", student.number, "::", student.course);
    }
  }
}

enum Course {
  case InformationTechnology
  case Multimedia
}

class Student {
  let number: Int;
  let name: String;
  let course: Course;

  init(number: Int, name: String, course: Course) {
    self.number = number;
    self.name = name;
    self.course = course;
  }
}


// var a6 = Classroom(capacity: 20, name: "Sala dos macs", number: 10);
// for i in 0...20 {
//   let student = Student(
//     number: Int(i),
//     name: "Student \(i)",
//     course: Course.InformationTechnology
//   )
//   if a6.addStudent(student) {
//     print("added with success");
//   } else {
//     print("Cant add");
//   }
// }
// 
// a6.list();
// if a6.removeStudent(25) {
//   print("removed");
// } else {
//   print("not found");
// }
// a6.list();
