import Foundation;

struct Transaction {
  let value: Int;
  let description: String;
}

struct ExpireDate {
  let month: Int;
  let year: Int;

  func isExpired() -> Bool {
    let calendar = Calendar.current
    let y = calendar.component(.year, from: Date());
    let m = calendar.component(.month, from: Date());

    if (self.year <= y && self.month < m) {
      return true;
    }

    return false;
  }
}

class CreditCard {
  var number: String = "";
  var name: String = "";
  var expireDate: ExpireDate = ExpireDate(month: 0, year: 0);
  var plafond: Int = 0;
  var totalSpent: Int = 0;
  var history: [Transaction] = [];

  init?(
    number: String,
    name: String,
    expireDate: ExpireDate,
    plafond: Int,
    totalSpent: Int = 0) {
    if self.validator(number: number) {
      self.number = number;
      self.name = name;
      self.expireDate = expireDate;
      self.plafond = plafond;
      self.totalSpent = totalSpent;
      return;
    }
    print("Invalid card number");
    return nil;
  }

  private func validator(number: String) -> Bool {
    let pattern = "[0-9]{12}"; 
    let result = number.range(of: pattern, options:.regularExpression);
    if result == nil {
      return false;
    }
    return true;
  }

  func balance() -> Int {
    return self.plafond - self.totalSpent;
  }

  func hasDebt() -> Bool {
    return self.totalSpent == 0;
  }

  func debt() -> Int {
    return self.totalSpent;
  }

  func payCredit(value: Int) {
    if value < self.totalSpent {
      self.totalSpent -= value;
    } else {
      print("your debt is not that big\r\n");
    }
  }

  func pay(value: Int, description: String) {
    if (self.totalSpent + value) < self.plafond {
      if !self.expireDate.isExpired() {
        self.totalSpent += value;
        self.history.append(Transaction(value: value, description: description))
      } else {
        print("your card has expired\r\n");
      }
    } else {
      print ("not enough money\r\n");
    }
  }

  func receipt(_ idx: Int = -1) -> String {
    let transaction = idx == -1 ?
      self.history.last :
      self.history[idx];

    if transaction != nil {
      return "\(transaction!.value) \(transaction!.description)";
    }
    return "transaction not found\r\n";
  }

  func getTransactions() -> String {
    var str = "";
    for (idx, _) in self.history.enumerated() {
      str += "\(self.receipt(idx))\r\n";
    }
    return str;
  }

  func toString() -> String {
    var str = "";
    str += "Holder: \(self.name)\r\n";
    str += "Number: \(self.number)\r\n";
    str += "Expire date: \(self.expireDate.month)/\(self.expireDate.year)\r\n";
    str += "Max value: \(self.plafond)\r\n";
    str += "Total Spent: \(self.totalSpent)\r\n";
    return str;
  }
}

class Wallet {
  let name: String;
  let address: String;
  let phone: String;
  var cards: [CreditCard] = [];

  init(name: String, address: String, phone: String) {
    self.name = name;
    self.address = address;
    self.phone = phone;
  }

  func saveCard(card: CreditCard) {
    self.cards.append(card);
  }

  func listCards() -> String {
    var str = "";
    for card in self.cards {
      str += "===============\r\n";
      str += card.toString();
      str += "===============\r\n";
    }
    return str;
  }

  func removeCard(number: String) {
    for (idx, card) in self.cards.enumerated() {
      if card.number == number && !card.hasDebt() {
        self.cards.remove(at: idx);
        return;
      }
    }
  }

  func countCards() -> Int {
    return self.cards.count;
  }

  func totalDebt() -> Int {
    return self.cards.map({ $0.debt() }).reduce(0, +);
  }

  func allTransactions() -> String {
    var str = "";
    for card in self.cards {
      str += "===========\r\n";
      str += "\(card.number)\r\n";
      str += "Transactions:\r\n";
      str += "\(card.getTransactions())"
      str += "===========\r\n";
    }
    return str;
  }

  func toString() -> String {
    var str = "";
    str += "\(self.name) - \(self.address) - \(self.phone)";
    str += self.listCards();
    str += "Total debt: \(self.totalDebt())";
    return str;
  }
}

// var wallet = Wallet(name: "Nuno Jesus", address: "Rua A", phone: "91111111");
// 
// if let card1 = CreditCard(
//   number: "123123123123",
//   name: "Nuno Jesus",
//   expireDate: ExpireDate(month: 11, year: 2020),
//   plafond: 1000
// ) {
//   wallet.saveCard(card: card1);
//   print("TEST CASE 1");
//   print(card1.balance());
//   card1.pay(value: 900, description: "laptop");
//   card1.pay(value: 110, description: "food");
//   card1.payCredit(value: 100);
//   print(card1.balance());
//   card1.pay(value: 110, description: "food");
//   print(card1.balance());
//   print(card1.receipt());
//   print(card1.getTransactions());
//   print(card1.toString());
// }
// 
// 
// if let card2 = CreditCard(
//   number: "123123123123",
//   name: "Nuno Jesus",
//   expireDate: ExpireDate(month: 09, year: 2020),
//   plafond: 1000
// ) {
//   wallet.saveCard(card: card2);
//   card2.pay(value: 100, description: "gas");
// }
// 
// 
// print("=====wallet=====\r\n");
// print(wallet.listCards());
// print(wallet.countCards());
// print(wallet.totalDebt())
// print(wallet.allTransactions())
// print(wallet.toString());
